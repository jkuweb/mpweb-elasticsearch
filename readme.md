# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.png)

# Descripción
-----------------------

Funcionamiento básico de filtros en ElasticSearch

Ejemplo de cómo filtrar documentos en ES, mostrando la diferencia entre una consulta tipo `match` (búsqueda exacta) o `fuzzy` (búsqueda similar).


# Instalación
-----------------------

```
$ vagrant up
```


# Instrucciones
-----------------------

- Entra en la máquina por ssh con `vagrant ssh`
- Entra en el directorio src y ejecuta `composer update`
- Ejecuta `curl -XPOST 'localhost:9200/bank/account/_bulk?pretty&refresh' --data-binary "@accounts.json"` para cargar los datos de test en ES
- Entra en el directorio `src/examples` y mira cada uno de los ejemplos, pudiendo cambiar los datos en ellos. Puedes ejecutarlos para ver tanto la petición hecha a ES, así como la respuesta.
- Inserta varios documentos en ES mediante `examples/01_insert_documents.php` y mira como la versión aumenta
- Dentro de la máquina de Vagrant, entra en el directorio ~/src y ejecuta el comando 'php -S 0.0.0.0:8080' para iniciar un servidor web en este directorio, y con puerto de origen 8080
- Entra en la URL 'http://1.2.3.4:8080/' para ver un formulario para buscar nombres de cuentas.
- Busca los clientes con nombre `martin`
- Puedes ver en la parte inferior de la página la consulta que se ha lanzado contra ES
- Ahora descomenta la siguiente línea del fichero `results_table.php`: `//$matchType = new SearchFuzzyMatch();`
- Vuelve a buscar los clientes con nombre `martin`


# Desinstalación
-----------------------

```
$ vagrant destroy
```
